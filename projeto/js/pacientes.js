//var botaoCalculaIMCs = document.getElementById("calcula-imcs");
var btnCalculaIMCs = document.querySelector("#calcula-imcs");

btnCalculaIMCs.addEventListener("click", function () {

	percorreLista(buscaTabPacientes(), function(pacienteAtual) {

		var paciente = convertePaciente(pacienteAtual);

		pacienteAtual.querySelector(".info-imc").textContent = paciente.getIMC();
	});
});


var btnAdicionaPaciente = document.querySelector("#adicionar-paciente");
btnAdicionaPaciente.addEventListener("click", function(event) {
		//impede comportamento padrao do componente, no caso de recarregar a pagina
		event.preventDefault(); 
		var tabelaPacientes = buscaPacientes();
		var novoPaciente = montaPaciente();
		tabelaPacientes.innerHTML += novoPaciente;
		limpaNovoPaciente();
	});


function buscaTabPacientes() {
	//return document.getElementsByClassName("paciente");
	//console.log(document.getElementsByClassName("paciente"));
	return document.querySelectorAll(".paciente");
}

function convertePaciente(pacienteAtual) {
	var paciente = {
		nome : pacienteAtual.querySelector(".info-nome").textContent,
		peso : pacienteAtual.querySelector(".info-peso").textContent,
		altura : pacienteAtual.querySelector(".info-altura").textContent,
		getIMC : function() {
			if(this.altura == 0) {
				console.log("Erro calculaImc()! Altura = 0 "
					.concat(" do ", paciente.nome));
			} else {
				return (this.peso / (this.altura * this.altura) ).toFixed(2);
			}
		}
	};

	return paciente;
}

function buscaPacientes() {
	return document.querySelector("table");
}

function montaPaciente() {
	var paciente = {
		nome : document.querySelector("#campo-nome").value,
		peso : document.querySelector("#campo-peso").value,
		altura : document.querySelector("#campo-altura").value,
	};
	var novoPaciente = "<tr class='paciente'>"+
	"<td class='info-nome'>" + paciente.nome + "</td>"+
	"<td class='info-peso'>" + paciente.peso + "</td>"+
	"<td class='info-altura'>" + paciente.altura + "</td>"+
	"<td class='info-imc'>" + 
	(paciente.peso / (paciente.altura * paciente.altura) ).toFixed(2) + "</td>"+
	"</tr>";
	return novoPaciente;
}

function limpaNovoPaciente() {
	document.querySelector("#campo-nome").value = "";
	document.querySelector("#campo-peso").value = "";
	document.querySelector("#campo-altura").value = "";
}

function exibeNomeDosPacientes() {
	percorreLista(buscaTabPacientes(), function(pacienteAtual) {
		var paciente = convertePaciente(pacienteAtual);
		console.log(paciente.nome);
	});
}

/*
function montaPaciente() {
	var paciente = "<tr class='paciente'>"+
	"<td class='info-nome'>Stuart</td>"+
	"<td class='info-peso'>50</td>"+
	"<td class='info-altura'>1.50</td>"+
	"<td class='info-imc'></td>"+
	"</tr>";
	return paciente;
}
*/

/*
function calculaIMCDosPacientes() {

	percorreLista(buscaTabPacientes(), function(pacienteAtual) {

		var paciente = convertePaciente(pacienteAtual);

		pacienteAtual.getElementsByClassName("info-imc")[0]
			.textContent = paciente.getIMC();
	});
}
*/

/*
O problema de se utilizar essa abordagem é que nós só conseguimos adicionar 
uma função por evento, ou seja, não conseguimos efetuar mais de uma função 
no clique de um usuário. 
*/

//botaoCalculaIMCs.onclick = calculaIMCDosPacientes;

