function buscaTrsClientes() {
	return document.getElementsByTagName("tr");
}

percorreLista(buscaTrsClientes(), function(trAtual) {
	trAtual.addEventListener("mouseover", function() {
		this.setAttribute("bgcolor", "grey");
	});
	trAtual.addEventListener("mouseout", function() {
		this.setAttribute("bgcolor", "white");
	});
	trAtual.addEventListener("click", function() {
		this.setAttribute("bgcolor", "red");
	});
});