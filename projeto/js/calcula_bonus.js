var salarioMensal = 2001;
var bonus;
var taxaBonus;
var salarioFinal;
var clt = true;
var estagiario = false;

function calculaSalarioFinal(salario, taxa) {
	return salario * (1 + taxa);
}

function calculaBonus(salario, taxa) {
	return salario * taxa;
}

if(estagiario || clt) {
	if(salarioMensal > 0) {
		if(salarioMensal <= 1000) {
			console.log("salarioMensal > 0 && salarioMensal <= 1000");
			taxaBonus = 0.05;
		} else if (salarioMensal <= 2000) {
			console.log("salarioMensal <= 2000");
			taxaBonus = 0.1;
		} else {
			console.log("salarioMensal > 2000");
			taxaBonus = 0.15;
		}

		bonus = calculaBonus(salarioMensal, taxaBonus);

		salarioFinal = calculaSalarioFinal(salarioMensal, taxaBonus);

		console.log("Salário: " . concat(salarioMensal));
		console.log("O bônus é: " . concat(taxaBonus * 100, "%, ou ", bonus.toFixed(2)));
		console.log("Salário + bônus é : " . concat( salarioFinal.toFixed(2) ) );
	} else {
		console.log("Salário negativo ou igual a 0 não é calculado bônus!");
	} 
} else {
	console.log("O Profissional precisa ser CLT ou Estagiário!");
} 




