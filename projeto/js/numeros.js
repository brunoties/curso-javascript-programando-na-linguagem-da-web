var numeros = [1, 2, 3, 4, 5];

function percorreArrays(lista, funcao) {
	for(var i = 0; i < lista.length; i++) {
		funcao(lista[i]);
	}
}

function exibeTodosNumeros() {
	percorreArrays(numeros, function(numero) {
		console.log(numero);
	});
}

exibeTodosNumeros();